mod lib;
use crate::lib::somelib::{add, sub};

fn main() {
    println!("Hello, world!");
    println!("{} + {} = {}", -1, 1, add(-1, 1));
    println!("{} - {} = {}", 1, 1, sub(1, 1));
}
