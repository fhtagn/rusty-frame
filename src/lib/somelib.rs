pub fn add(a: isize, b: isize) -> isize {
    a + b
}

pub fn sub(a: isize, b: isize) -> isize {
    a - b
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn _add() {
        assert_eq!(0, add(-1, 1));
    }

    #[test]
    fn _sub() {
        assert_eq!(0, sub(1, 1));
    }
}
